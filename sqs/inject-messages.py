#!/usr/bin/env python

import Queue
import math
import multiprocessing
import os
import threading
import time
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../boto'))

import boto
from boto.sqs.queue import Queue as SqsQueue

class ThreadBatchSend(threading.Thread):
    """Threaded SQS Message Send"""
    def __init__(self,
                 thread_queue,
                 aws_access_key_id,
                 aws_secret_access_key,
                 region,
                 sqs_queue_url):
        threading.Thread.__init__(self)

        self.thread_queue  = thread_queue
        self.sqs_conn      = boto.sqs.connect_to_region(region,
                                                        aws_access_key_id=aws_access_key_id,
                                                        aws_secret_access_key=aws_secret_access_key)
        self.sqs_queue     = SqsQueue(connection=self.sqs_conn, url=sqs_queue_url)

    def run(self):
        while True:

            #grabs the next batch to send from queue
            batch = self.thread_queue.get()

            if len(batch) == 1:
                self.sqs_conn.send_message(queue=self.sqs_queue, message_content=batch[0][1])
            else:
                self.sqs_conn.send_message_batch(queue=self.sqs_queue, messages=batch)

            #signals to queue job is done
            self.thread_queue.task_done()


def inject_messages(aws_access_key_id,
                    aws_secret_access_key,
                    region,
                    queue_url,
                    num_messages,
                    batch_size,
                    num_processes,
                    num_threads_per_process):

    num_messages_per_process       = float(num_messages) / num_processes
    num_messages_already_allocated = 0
    processes                      = []

    for i in xrange(num_processes):
        num_messages_for_this_process   = int(round((i+1) * num_messages_per_process - num_messages_already_allocated))
        process = multiprocessing.Process(target=inject_messages_using_single_process,
                                          kwargs={
                                                'process_num':           i+1,
                                                'aws_access_key_id':     aws_access_key_id,
                                                'aws_secret_access_key': aws_secret_access_key,
                                                'region':                region,
                                                'queue_url':             queue_url,
                                                'num_messages':          num_messages_for_this_process,
                                                'batch_size':            batch_size,
                                                'num_threads':           num_threads_per_process,
                                          })
        processes.append(process)
        print "Allocated %d message(s) to process #%d/%d" % (num_messages_for_this_process, i+1, num_processes)
        num_messages_already_allocated += num_messages_for_this_process

    print ""
    print "Allocated a total of %d message(s) to %d process(es)" % (num_messages_already_allocated, num_processes)
    print ""


    start_time = time.time()

    for process in processes:
        process.start()

    # Wait for all the processes to finish
    for process in processes:
        process.join()

    total_time = time.time() - start_time

    if num_processes >= 2:
        print ""
        print "Using %d parallel processes: Injected %s message(s) in %.3f seconds (%.1f messages per second)" % (
            num_processes,
            num_messages,
            total_time,
            num_messages / total_time
        )
        print ""


def inject_messages_using_single_process(process_num,
                                         aws_access_key_id,
                                         aws_secret_access_key,
                                         region,
                                         queue_url,
                                         num_messages,
                                         batch_size,
                                         num_threads):

    thread_queue = Queue.Queue()

    #spawn a pool of threads, and pass them the queue instance
    for i in xrange(max(1, num_threads)):
      thread = ThreadBatchSend(thread_queue          = thread_queue,
                               aws_access_key_id     = aws_access_key_id,
                               aws_secret_access_key = aws_secret_access_key,
                               region                = region,
                               sqs_queue_url         = queue_url)
      thread.setDaemon(True)
      thread.start()

    batch = []

    num_batches          = int(math.ceil(float(num_messages) / batch_size))
    batch_num            = 0
    num_bytes_this_batch = 0

    start_time           = time.time()

    for i in xrange(num_messages):
        body = 'This is test message injection.' + ('Large message lorem ipsum blah blah blah...' * 30) + '  #%s/%s' % ((i + 1), num_messages)
        num_bytes_this_batch += len(body)
        batch.append([i, body, 0])
        if len(batch) >= batch_size or i + 1 >= num_messages:
            batch_num += 1
            thread_queue.put(batch)
            print 'Added %d message(s) in batch #%d/%d (%d bytes per message)' % (len(batch), batch_num, num_batches, float(num_bytes_this_batch) / len(batch))
            batch = []
            num_bytes_this_batch = 0

    print 'Waiting for all messages to be sent to SQS...'

    #wait on the queue until everything has been processed
    thread_queue.join()

    total_time = time.time() - start_time

    print ""
    print "In Process %d: Injected %s message(s) in %.3f seconds (%.1f messages per second)" % (
        process_num,
        num_messages,
        total_time,
        num_messages / total_time
    )

    # Giving the threads a little time to shut down properly
    sleep_ms = 20
    time.sleep(sleep_ms / 1000.0)


def get_args_from_cli():
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('--aws-access-key-id',       required=True,  help="AWS Access Key ID")
    parser.add_argument('--aws-secret-access-key',   required=True,  help="AWS Secret Access Key")
    parser.add_argument('--region',                  required=True,  help="The AWS region (e.g. ap-southeast-2)")
    parser.add_argument('--queue-url',               required=True,  help="The SQS queue URL")
    parser.add_argument('--num-messages',            required=True,  help="The Number of messages to inject",     type=int)
    parser.add_argument('--batch-size',              required=False, help="Batch size when sending messages",     type=int, default=10)
    parser.add_argument('--num-processes',           required=False, help="Number of processes to use",           type=int, default=1)
    parser.add_argument('--num-threads-per-process', required=False, help="Number of threads to use per process", type=int, default=5)

    args = parser.parse_args()

    return vars(args)


if __name__ == '__main__':
    inject_messages(**get_args_from_cli())
