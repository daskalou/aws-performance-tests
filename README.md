# AWS Performance tests

Quickly test the performance of various Amazon AWS components

## SQS message injection

Use `sqs/inject-messages.py` to test how fast you can inject into an SQS queue.

Usage:

```
python sqs/inject-messages.py [-h] \
  --aws-access-key-id        AWS_ACCESS_KEY_ID \
  --aws-secret-access-key    AWS_SECRET_ACCESS_KEY \
  --region                   REGION \
  --queue-url                QUEUE_URL \
  --num-messages             NUM_MESSAGES \
  [--batch-size              BATCH_SIZE] \
  [--num-processes           NUM_PROCESSES] \
  [--num-threads-per-process NUM_THREADS_PER_PROCESS]

optional arguments:
  -h, --help            show this help message and exit
  --aws-access-key-id AWS_ACCESS_KEY_ID
                        AWS Access Key ID
  --aws-secret-access-key AWS_SECRET_ACCESS_KEY
                        AWS Secret Access Key
  --region REGION       The AWS region (e.g. ap-southeast-2)
  --queue-url QUEUE_URL
                        The SQS queue URL
  --num-messages NUM_MESSAGES
                        The Number of messages to inject
  --batch-size BATCH_SIZE
                        Batch size when sending messages
  --num-processes NUM_PROCESSES
                        Number of processes to use
  --num-threads-per-process NUM_THREADS_PER_PROCESS
                        Number of threads to use per process
```
